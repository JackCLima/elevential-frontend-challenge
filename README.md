# README #

#1. O que você achou do desafio?

Muito construtivo, me forçou a pesquisar e me aprofundar nas tecnologias mesmo que não tenha sido concluído, como eu gostaria, 
foi muito bom trabalhar com uma área que tenho que buscar atingir o melhor da performance, tanto visual quanto de código, 
tendo que refletir constantemente sobre as decisões tomadas anteriormente. 

#2. Quais foram as maiores dificuldades encontradas?

As tomadas de decisões em relação a tecnologias a serem usadas. 
A gestão da codificação e a  pesquisa por exemplos de performance e padrões de mercado.

#3. O que poderia ser melhorado no resultado?

A interface gráfica final, e mais verificações coerentes em cada campo de entrada a ser preenchido.

#4. Quais funcionalidades você  julgou mais importantes e por quê?

Importância visual e coleta de dados andam juntas, e são de extrema importância, 
porém me foquei na entrada de dados por ser o “motor ativo”. 
Não é nem um pouco bom ter uma interface gráfica ruim, 
mas é impossível a aplicação funcionar sem uma coleta de dados bem feita.   

#5. Que decisões você tomou e por quê?

Decidi focar primeiro no html, Js e na coleta dos dados, para garantir a funcionalidade maios da aplicação, 
depois após observar a parte gráfica tive a errônea ideia de não utilizar nenhum biblioteca ou ferramenta que 
ajuda-se a interface, por subestimar a interface, cometi muitos erros, foi preciso utilizar “técnicas” com .CSS não agradáveis, 
além de vir a acabar gerando divs desnecessárias e atrapalhar o desenvolvimento do projeto como um todo 
(erro que não pretendo cometer mais, na vida),  no demais foi um grande aprendizado tanto nos acertos quanto nos erros,
com certeza tenho uma visão melhor do que é fazer um aplicação deste tipo agora do que antes do desafio.